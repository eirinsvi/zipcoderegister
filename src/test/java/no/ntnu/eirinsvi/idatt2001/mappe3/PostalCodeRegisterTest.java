package no.ntnu.eirinsvi.idatt2001.mappe3;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class that tests the addZipCode method in the singleton class PostalCodeRegister
 */
public class PostalCodeRegisterTest {
    PostalCode testPostalCode = new PostalCode("0707", "Ås", "Bærum");


    @Nested
    public class addPostalCodeMethodWorksWithExistingAndNonExistingPostalCode {

        /**
         * Positive testing
         * Test that checks that a Postal Code who does not already exist
         * in the  PostalCodeRegister gets added to the register
         */
        @Test
        @DisplayName("add a new postal code in the register")
        public void addPostalCodeWhoDoesNotExistInRegister() throws AddPostalCodeException {
            PostalCodeRegister.getInstance().getPostalCodes().clear();
            PostalCodeRegister.getInstance().addZipCode(testPostalCode);
            assertEquals(testPostalCode, PostalCodeRegister.getInstance().getPostalCodes().get(0));
        }

        /**
         * negative testing
         * Test that checks that when a Postal Code who already exist
         * in the postalCodeRegister gets added to the register
         * it throws an AddPostalException
         */
        @Test
        @DisplayName("add an already existing postal code in the register")
        public void addExistingPostalCodeInRegister() throws AddPostalCodeException {
            PostalCodeRegister.getInstance().getPostalCodes().clear();
            PostalCodeRegister.getInstance().addZipCode(testPostalCode);
            assertThrows(AddPostalCodeException.class, () -> PostalCodeRegister.getInstance().addZipCode(testPostalCode));
        }

        /**
         * negative testing
         * Test that checks that when a Postal Code who dont have a four characters postal code
         * gets added to the register it will throw an IllegalArgumentException
         */
        @Test
        @DisplayName("add postal code where the zip code do not have four characters")
        public void addPostalCodeWithMoreThanFourCharacters() throws IllegalArgumentException {
            assertThrows(IllegalArgumentException.class, () -> PostalCodeRegister.getInstance().addZipCode(new PostalCode("12345", "Ås", "Bærum")));
        }
    }
}
