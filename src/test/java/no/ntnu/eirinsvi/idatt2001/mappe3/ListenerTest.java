package no.ntnu.eirinsvi.idatt2001.mappe3;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing the interface Listener and filterListener who implements Listener
 */
public class ListenerTest {
    Listener listener = new filterListener();
    ObservableList<PostalCode> allPostalCodes = FXCollections.observableArrayList();
    ObservableList<PostalCode> filteredPostalCodes = FXCollections.observableArrayList();
    PostalCode test = new PostalCode("1234","oslo","oslo");
    String filter = "o";
    String filter2 = "2";
    String filter3 = "v";

    @Nested
    public class filterTakesInCorrectPostalCodes {

        /**
         * Positive testing
         * Test that checks that a Postal Code gets added
         * to filteredPostalCodes when the postal code matches the filter
         */
        @Test
        @DisplayName("filteredPostalCodes takes in correct postal code based on the postal code")
        public void filteredPostalCodesTakesInCorrectZipCodeBasedOnPostalCode() {
            allPostalCodes.add(test);
            filteredPostalCodes = listener.update(allPostalCodes, filter2);
            assertEquals(test, filteredPostalCodes.get(0));
        }

        /**
         * Positive testing
         * Test that checks that a Postal Code gets added
         * to filteredPostalCodes when the post location matches the filter
         */
        @Test
        @DisplayName("filteredPostalCodes takes in correct postal code based on the post location")
        public void filteredPostalCodesTakesInCorrectZipCodeBasedOnPostLocation() {
            allPostalCodes.clear();
            allPostalCodes.add(test);
            filteredPostalCodes = listener.update(allPostalCodes, filter);
            assertEquals(test, filteredPostalCodes.get(0));
        }

        /**
         * negative testing
         * Test that checks that a Postal Code do not get added
         * to filteredPostalCodes when neither the postal code
         * or post location matches the filter
         */
        @Test
        @DisplayName("filteredPostalCodes do not take in postal code who do not match the filter")
        public void filteredPostalCodesDoNotTakeInPostalCodeWhoDoNotMatchesFilter() {
            allPostalCodes.clear();
            allPostalCodes.add(test);
            filteredPostalCodes = listener.update(allPostalCodes, filter3);
            assertFalse(filteredPostalCodes.size() > 0);
        }
    }
}
