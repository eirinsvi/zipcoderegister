package no.ntnu.eirinsvi.idatt2001.mappe3;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing the class FileHandler
 */
public class FileHandlerTest {

    /**
     * Method for testing if reading a file works,
     * and the postal codes in the file gets added to the PostalCodeRegister
     */
    @Test
    public void readFile() throws IOException, AddPostalCodeException {
        File file = new File("src\\main\\resources\\ZipCodeRegister.txt");

        PostalCodeRegister.getInstance().getPostalCodes().clear();
        FileHandler.importFromFile(file);

        assertTrue(PostalCodeRegister.getInstance().getPostalCodes().size()>0);
    }
}
