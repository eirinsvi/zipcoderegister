package no.ntnu.eirinsvi.idatt2001.mappe3;

import javafx.collections.ObservableList;

public interface Listener {
    ObservableList<PostalCode> update(ObservableList<PostalCode> allPostalCodes, String filter);
    boolean matchesFilter(PostalCode postalCode, String filter);
}
