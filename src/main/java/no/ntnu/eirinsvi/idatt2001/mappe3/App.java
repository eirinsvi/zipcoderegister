package no.ntnu.eirinsvi.idatt2001.mappe3;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;

/**
 * JavaFX App
 */
public class App extends Application {

    MainController controller;
    TableView<PostalCode> tableView;
    TextField filterString;
    Listener listener;
    ObservableList<PostalCode> allPostalCodes;

    /**
     * method that runs after the init method
     * Creating the BorderPane and setting all the nodes
     * @param stage
     */
    @Override
    public void start(Stage stage) {
        BorderPane borderPane = (BorderPane) NodeFactory.create("borderpane");
        VBox vBox = (VBox) NodeFactory.create("vbox");

        MenuBar menuBar = createMenuBar();
        vBox.getChildren().add(menuBar);

        ToolBar toolBar = createToolBar();
        vBox.getChildren().add(toolBar);

        tableView = createTableView();
        borderPane.setCenter(tableView);

        borderPane.setTop(vBox);

        listener.update(allPostalCodes, filterString.getText().toLowerCase());

        stage.setTitle("Postal Code Register");
        stage.setScene(new Scene(borderPane, 600, 480));
        stage.setResizable(false);

        stage.show();
    }

    /**
     * method that runs when the application is opened
     * @throws Exception
     */
    @Override
    public void init() throws Exception {
        super.init();

        controller = new MainController(this);
        listener = new filterListener();
        allPostalCodes = FXCollections.observableArrayList();

        //Importing the file with the postal codes and setting the PostalCodeRegister
        try {
            FileHandler.importFromFile(new File(("src\\main\\resources\\ZipCodeRegister.txt")));
        } catch (AddPostalCodeException e) {
            e.getMessage();
        }
        allPostalCodes = PostalCodeRegister.getInstance().getPostalCodes();
    }

    /**
     * method that creates the tableview, adding all the columns and sets the items
     * @return the new tableview
     */
    private TableView<PostalCode> createTableView(){
        TableView<PostalCode> tableView = (TableView<PostalCode>) NodeFactory.create("tableview");

        TableColumn<PostalCode, String> postalCodeColumn = new TableColumn<>("Postal code");
        TableColumn<PostalCode, String> postLocationColumn = new TableColumn<>("Post location");
        TableColumn<PostalCode, String> communeNameColumn = new TableColumn<>("Commune name");

        postalCodeColumn.setCellValueFactory(new PropertyValueFactory<>("zipCode"));
        postLocationColumn.setCellValueFactory(new PropertyValueFactory<>("postLocation"));
        communeNameColumn.setCellValueFactory(new PropertyValueFactory<>("communeName"));

        tableView.getColumns().addAll(postalCodeColumn,postLocationColumn,communeNameColumn);

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.setItems(listener.update(allPostalCodes, filterString.getText().toLowerCase()));

        return tableView;
    }

    /**
     * method that creates the menubar, its menus, items and what happens when you click them
     * @return the new menu
     */
    private MenuBar createMenuBar(){
        MenuBar menuBar = (MenuBar) NodeFactory.create("menubar");

        Menu exitMenu = new Menu("Exit");
        Menu infoMenu = new Menu("Info");

        MenuItem closeApplication = new MenuItem("Close application");
        MenuItem about = new MenuItem("About");
        MenuItem help = new MenuItem("Help");

        exitMenu.getItems().add(closeApplication);
        infoMenu.getItems().addAll(about, help);

        closeApplication.setOnAction(event -> controller.closeApp());
        about.setOnAction(event -> controller.clickedAbout());
        help.setOnAction(event -> controller.clickedHelp());

        menuBar.getMenus().addAll(exitMenu,infoMenu);

        return menuBar;
    }

    /**
     * method that creates the toolbar
     * places the label and textfield in the toolbal
     * adding listener to the textfield
     * @return the new toolbar
     */
    private ToolBar createToolBar(){
        ToolBar toolBar = (ToolBar) NodeFactory.create("toolbar");

        Label filterInformation = (Label) NodeFactory.create("label");
        filterInformation.setText("Search for zip code or post location:");

        filterString = (TextField) NodeFactory.create("textfield");
        filterString.setPromptText("zip code or post location");
        filterString.textProperty().addListener((observable, oldValue, newValue) -> listener.update(allPostalCodes, filterString.getText().toLowerCase()));

        toolBar.getItems().addAll(filterInformation, filterString);

        return toolBar;
    }
}