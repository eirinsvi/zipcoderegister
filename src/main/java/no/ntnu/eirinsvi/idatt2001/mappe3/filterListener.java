package no.ntnu.eirinsvi.idatt2001.mappe3;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class for handling the filter
 */
public class filterListener implements Listener{
    private ObservableList<PostalCode> filteredPostalCodes = FXCollections.observableArrayList();

    /**
     * Method for adding zip codes that matches the filtering to filteredZipCodes
     * @param allPostalCodes all the postal codes from the register
     * @param filter the filter applied
     * @return the list with the filtered postal codes
     */
    @Override
    public ObservableList<PostalCode> update(ObservableList<PostalCode> allPostalCodes, String filter) {
        filteredPostalCodes.clear();

        for (PostalCode postalCode : allPostalCodes) {
            if (matchesFilter(postalCode, filter)) {
                filteredPostalCodes.add(postalCode);
            }
        }
        return filteredPostalCodes;
    }

    /**
     * Method for checking if zip code matches filter.
     * Lower/Upper case is ignored.
     * @param postalCode zip code to be checked if matches the filter
     * @param filter the filter applied
     * @return true if zip code matches filter or if there are not a filter applied. If not, it returns false.
     */
    @Override
    public boolean matchesFilter(PostalCode postalCode, String filter) {
        if (filter == null || filter.isEmpty()) {
            return true;
        }

        if (postalCode.getZipCode().toLowerCase().indexOf(filter) != -1) {
            return true;
        } else if (postalCode.getPostLocation().toLowerCase().indexOf(filter) != -1) {
            return true;
        }
        return false;
    }
}
