package no.ntnu.eirinsvi.idatt2001.mappe3;

/**
 * class who handles one zip code
 */
public class PostalCode {
    private String zipCode;
    private String postLocation;
    private String communeName;


    /**
     * A constructor that creates the patient
     * @param zipCode the postal codes zip code.
     * Must be 4 characters/digits
     * @param postLocation the postal codes post location
     * @param communeName the postal codes commune name
     */
    public PostalCode(String zipCode, String postLocation, String communeName) {
        //Checking if the zip code has four digits/characters
        setZipCode(zipCode);
        this.postLocation = postLocation;
        this.communeName = communeName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String newZipCode) {
        if(newZipCode.length()==4 ) {
            zipCode = newZipCode;
        } else{
            throw new IllegalArgumentException("Zip code must be 4 digits.");
        }
    }

    public String getPostLocation() {
        return postLocation;
    }

    public String getCommuneName() {
        return communeName;
    }

    @Override
    public String toString() {
        return "{" +
                "zipCode=" + zipCode +
                ", postLocation='" + postLocation + '\'' +
                ", communeName='" + communeName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PostalCode)) return false;
        PostalCode postalCode1 = (PostalCode) o;
        return zipCode == postalCode1.zipCode &&
                postLocation.equals(postalCode1.postLocation) &&
                communeName.equals(postalCode1.communeName);
    }
}
