package no.ntnu.eirinsvi.idatt2001.mappe3;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileHandler {

    /**
     * method that reads a file and adds the zip code to the zip code register
     * @param file file to read
     * @throws IOException
     * @throws AddPostalCodeException
     */
    public static void importFromFile(File file) throws AddPostalCodeException, IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
        PostalCodeRegister.getInstance().getPostalCodes().clear();
        String textLine;
        bufferedReader.readLine();
        while ((textLine = bufferedReader.readLine()) != null) {
            String[] zipCodeString = textLine.split("\t");
            PostalCode postalCode = new PostalCode(zipCodeString[0], zipCodeString[1], zipCodeString[3]);
            PostalCodeRegister.getInstance().addZipCode(postalCode);
        }
    }
}
