package no.ntnu.eirinsvi.idatt2001.mappe3;

/**
 * AddPostalCodeException takes in the AddPostalCodeException which gets thrown in the addZipCode method in PostalCodeRegister.
 */
public class AddPostalCodeException extends Throwable {
    private static final long serialVersionUID = 1L;

    public AddPostalCodeException(String errorMessage){
        super("Postal code is not supported: " + errorMessage);
    }
}
