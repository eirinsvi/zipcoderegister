package no.ntnu.eirinsvi.idatt2001.mappe3;

import java.io.File;
import java.io.IOException;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;

/**
 * Controller class for the application
 */
public class MainController {
    App application;


    public MainController(App application) {
        this.application = application;
    }

    /**
     * Method to close the application
     */
    public void closeApp(){
        Platform.exit();
    }

    /**
     * Method called when you push the menuitem about in the menubar
     * Method showing an information alert with information about the application
     */
    public void clickedAbout(){
        Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
        equalFoundAlert.setTitle("Information Dialog - About");
        equalFoundAlert.setHeaderText("Postal Code Register\nv0.1-SNAPSHOT");
        equalFoundAlert.setContentText("Application created by\nEirin Svinsås\n2021-05-14");
        equalFoundAlert.showAndWait();
    }

    /**
     * Method called when you push the menuitem help in the menubar
     * Method showing an information alert with a description on how to use the application
     */
    public void clickedHelp(){
        Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
        equalFoundAlert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(node -> ((Label)node).setMinHeight(Region.USE_PREF_SIZE));
        equalFoundAlert.setTitle("Information Dialog - Help");
        equalFoundAlert.setHeaderText("Thank you for using the Postal Code Register!\nHere are some tips to help you get started.");
        equalFoundAlert.setContentText("The application list out all the registered postal codes in Norway." +
                "\n\u2000\nBy clicking the table column names you may sort the Postal codes different ways.\n\u2000\n" +
                "You may also use the filter text box to search for spescific postal codes or post location.\n\u2000\n" +
                "If you do not remember the entire number or location, no worries! " +
                "You may also search for parts of the number or location.");
        equalFoundAlert.showAndWait();
    }
}

