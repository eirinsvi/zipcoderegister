package no.ntnu.eirinsvi.idatt2001.mappe3;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class PostalCodeRegister {

    private static PostalCodeRegister postalCode;
    private ObservableList<PostalCode> postalCodes = null;

    public static PostalCodeRegister getInstance() {
        if(postalCode == null){
            postalCode = new PostalCodeRegister();
        }
        return postalCode;
    }

    private PostalCodeRegister(){
        postalCodes= FXCollections.observableArrayList();
    }

    public ObservableList<PostalCode> getPostalCodes(){
        return this.postalCodes;
    }

    public void addZipCode(PostalCode postalCode) throws AddPostalCodeException {
        if(!(postalCodes.contains(postalCode))){
            postalCodes.add(postalCode);
        } else {
            throw new AddPostalCodeException("ZipCode already exist");
        }
    }
}
