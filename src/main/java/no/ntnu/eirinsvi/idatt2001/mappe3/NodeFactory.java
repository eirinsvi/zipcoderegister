package no.ntnu.eirinsvi.idatt2001.mappe3;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * Factory class that creates node objects needed in the application
 */
public class NodeFactory {
    public static Node create(String nodeName) {
        if (nodeName.isBlank()) {
            return null;
        }else if(nodeName.equalsIgnoreCase("tableview")){
            return new TableView<>();
        }else if(nodeName.equalsIgnoreCase("menubar")){
            return new MenuBar();
        }else if(nodeName.equalsIgnoreCase("toolbar")){
            return new ToolBar();
        }else if(nodeName.equalsIgnoreCase("borderpane")){
            return new BorderPane();
        }else if(nodeName.equalsIgnoreCase("vbox")){
            return new VBox();
        }else if(nodeName.equalsIgnoreCase("textfield")){
            return new TextField();
        }else if(nodeName.equalsIgnoreCase("label")){
            return new Label();
        }
        return null;
    }

}
